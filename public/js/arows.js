var mapping = {
  65: '.left',
  87: '.up',
  68: '.right',
  83: '.down'
}
x = window.innerWidth / 2 - 130 / 2,
  y = window.innerHeight / 2 - 130 / 2

$(document.documentElement).keydown(function (event) {
  var key = mapping[event.keyCode]
  if (key) $(key).addClass('pressed')

})

$(document.documentElement).keyup(function (event) {
  var key = mapping[event.keyCode]
  if (key) $(key).removeClass('pressed')
})
var up = false,
  right = false,
  down = false,
  left = false

document.addEventListener('keydown', press)
// $.ajaxSetup({
//   headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//   }
// })

// $.ajax({
//   type: "POST",
//   url: 'postControl',
//   dataType: 'json',
// })

function press (e) {
  if (e.keyCode === 38 /* up */ || e.keyCode === 87 /* w */) {
    postControl('w')
    up = true
  }
  if (e.keyCode === 39 /* right */ || e.keyCode === 68 /* d */) {
    postControl('d')
    right = true
  }
  if (e.keyCode === 40 /* down */ || e.keyCode === 83 /* s */) {
    postControl('s')
    down = true
  }
  if (e.keyCode === 37 /* left */ || e.keyCode === 65 /* a */) {
    postControl('a')
    left = true
  }
}

document.addEventListener('keyup', release)

function release (e) {
  if (e.keyCode === 38 /* up */ || e.keyCode === 87 /* w */) {
    up = false
  }
  if (e.keyCode === 39 /* right */ || e.keyCode === 68 /* d */) {
    right = false
  }
  if (e.keyCode === 40 /* down */ || e.keyCode === 83 /* s */) {
    down = false
  }
  if (e.keyCode === 37 /* left */ || e.keyCode === 65 /* a */) {
    left = false
  }
}

function sendData () {

  if (up) {
    document.getElementById('cam_control').innerHTML = 'Up'
  }
  if (right) {
    document.getElementById('cam_control').innerHTML = 'Right'
  }
  if (down) {
    document.getElementById('cam_control').innerHTML = 'Down'
  }

  if (left) {
    document.getElementById('cam_control').innerHTML = 'Left'
  }
  window.requestAnimationFrame(sendData)
}

window.requestAnimationFrame(sendData)