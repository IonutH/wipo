<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patient = Patient::all();

        return view('admin.patient.index')->with('patient', $patient);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $patient = Patient::all();

        return view('admin.patient.create')->with('patient', $patient);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patient = new Patient();

        if(!empty($request->first_name)){
            $patient->first_name = $request->first_name;
        }

        if(!empty($request->last_name)){
            $patient->last_name = $request->last_name;
        }

        if(!empty($request->age)){
            $patient->age = $request->age;
        }

        if(!empty($request->sex)){
            $patient->sex = $request->sex;
        }

        if(!empty($request->phone)){
            $patient->phone = $request->phone;
        }

        if(!empty($request->blood_type)){
            $patient->blood_type = $request->blood_type;
        }

        if(!empty($request->mail)){
            $patient->mail = $request->mail;
        }

        if(!empty($request->address)){
            $patient->address = $request->address;
        }

        if(!empty($request->observations)){
            $patient->observations = $request->observations;
        }


        if(!empty($request->image)){
            $profile_image = $request->image;
            $image_new_name = time() . $profile_image->getClientOriginalName();
            $profile_image->move('public/img/profile_image/', $image_new_name);
            $patient->image = 'public/img/profile_image/' . $image_new_name;
        }

        if($patient->save()){
            return redirect()->route('patient');
        }else{
            return response('Cannot save the patient', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::find($id);

        return view('admin.patient.edit')->with('patient', $patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::find($id);

        if(!empty($request->first_name)){
            $patient->first_name = $request->first_name;
        }

        if(!empty($request->last_name)){
            $patient->last_name = $request->last_name;
        }

        if(!empty($request->age)){
            $patient->age = $request->age;
        }

        if(!empty($request->sex)){
            $patient->sex = $request->sex;
        }

        if(!empty($request->blood_type)){
            $patient->blood_type = $request->blood_type;
        }

        if(!empty($request->phone)){
            $patient->phone = $request->phone;
        }

        if(!empty($request->mail)){
            $patient->mail = $request->mail;
        }

        if(!empty($request->address)){
            $patient->address = $request->address;
        }

        if(!empty($request->observations)){
            $patient->observations = $request->observations;
        }

        if($patient->save()){
            return redirect()->route('patient');
        }else{
            return response('Cannot update the patient', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::find($id);
        if(!$patient){
            return 'nu avem ce sterge';
        }else{
            $patient->delete();
        }

        return redirect()->route('patient');
    }
}
