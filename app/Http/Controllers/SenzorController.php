<?php

namespace App\Http\Controllers;

use App\Control;
use App\Patient;
use App\Senzor;
use App\Wipo;
use Illuminate\Http\Request;

class SenzorController extends Controller
{
	public function allPersons()
	{
		return Patient::all();
	}

	public function fillWipo()
	{
		for ($i = 0; $i < 500; $i++) {
			$store = new Wipo();
			$store->pulse = random_int(0, 300);
			$store->temperatures = random_int(36, 38);
			$store->accelerometer = rand(0, 10) / 10;
			$store->gyroscope = rand(0, 10) / 10;
			$store->tension = rand(30, 42) / 10;

			$store->save();
		}
	}

    public function fillSenzor()
    {
        for ($i = 0; $i < 100; $i++) {
            $store = new Senzor();
            $store->steps = random_int(0, 8000);
            $store->rate = random_int(60, 140);
            $store->activity = random_int(0, 10000);
            $store->sleep = random_int(6, 10);

            $store->save();
        }
    }

	public function getCords()
	{
		return Wipo::inRandomOrder()
			->first();
	}

	public function postControl(Request $request)
	{
		$store = new Control();
		$store->direction = $request->direction;

		$store->save();
	}

	public function getControls()
	{
		return Control::orderBy('created_at', 'desc')->pluck('direction');
	}

	public function getBodyTemp()
	{
		return Wipo::inRandomOrder()
			->first();
	}

	public function getWipo()
	{
		return Wipo::all();
	}

	public function getPulse($pulse)
	{
		$store = new Wipo();
		$store->pulse = $pulse;

		$store->save();
	}

	public function getTemperatures($temperatures)
	{
		$store = new Wipo();
		$store->temperatures = $temperatures;

		$store->save();
	}

	public function getAccelerometer($accelerometer)
	{
		$store = new Wipo();
		$store->accelerometer = $accelerometer;

		$store->save();
	}

	public function getGyroscope($gyroscope)
	{
		$store = new Wipo();
		$store->gyroscope = $gyroscope;

		$store->save();
	}

	public function getTension($tension)
	{
		$store = new Wipo();
		$store->tension = $tension;

		$store->save();
	}

	public function getAllData($pulse, $temperatures, $accelerometer, $gyroscope, $tension)
	{
		$store = new Wipo();

		$store->pulse = $pulse;
		$store->temperatures = $temperatures;
		$store->accelerometer = $accelerometer;
		$store->gyroscope = $gyroscope;
		$store->tension = $tension;

		$store->save();
	}

}
