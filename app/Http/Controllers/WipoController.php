<?php

namespace App\Http\Controllers;

use App\Wipo;
use Illuminate\Http\Request;

class WipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wipo  $wipo
     * @return \Illuminate\Http\Response
     */
    public function show(Wipo $wipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wipo  $wipo
     * @return \Illuminate\Http\Response
     */
    public function edit(Wipo $wipo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wipo  $wipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wipo $wipo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wipo  $wipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wipo $wipo)
    {
        //
    }
}
