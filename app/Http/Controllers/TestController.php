<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index() {
        $test = Test::all();

        return $test;
//        return view('admin.test')->with('test', $test);
    }

    public function store(Request $request) {
        $test = new Test();

        $test->name = $request->name;

        $test->save();
    }
    
     public function store2(Request $request) {
        $test = new Test();

        $test->name = 'test';

        if($test->save())
        {
            return $test;
        }else{
            return 'problem';
        }
        
    }
}
