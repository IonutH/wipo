<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/foo', function () {
	Artisan::call('migrate:refresh', [
		'--force' => true,
	]);
});

Route::get('/', function () {
	return view('welcome');
});

Route::get('/admin', [
	'uses' => 'AdminController@index',
	'as' => 'admin'
]);

Route::post('/admin/postControl', [
	'uses' => 'SenzorController@postControl',
	'as' => 'postControl'
]);
Route::get('/admin/getControls', [
	'uses' => 'SenzorController@getControls',
	'as' => 'getControls'
]);
Route::get('/admin/fillWipo', [
	'uses' => 'SenzorController@fillWipo',
	'as' => 'fillWipo'
]);

Route::get('/admin/fillSenzor', [
	'uses' => 'SenzorController@fillSenzor',
	'as' => 'fillSenzor'
]);

Route::get('/admin/getCords', [
	'uses' => 'SenzorController@getCords',
	'as' => 'getCords'
]);
Route::get('/admin/getBodyTemp', [
	'uses' => 'SenzorController@getBodyTemp',
	'as' => 'getBodyTemp'
]);

Route::get('/admin/fillSenzor', [
    'uses' => 'SenzorController@fillSenzor',
    'as'   => 'fillSenzor'
]);

Route::get('/admin/fillSenzor', [
    'uses' => 'SenzorController@fillSenzor',
    'as'   => 'fillSenzor'
]);

Route::get('/admin/getWipo', [
	'uses' => 'SenzorController@getWipo',
	'as' => 'getWipo'
]);

Route::get('/admin/table', [
	'uses' => 'AdminController@table',
	'as' => 'admin.table'
]);

Route::get('/admin/interface', [
	'uses' => 'AdminController@interface',
	'as' => 'admin.interface'
]);

//Patient
Route::get('/admin/patient', [
	'uses' => 'PatientController@index',
	'as' => 'patient'
]);
//Route::post('/admin/patient', [
//    'uses' => 'PatientController@index',
//    'as'   => 'patient'
//]);
Route::get('/admin/patient/create', [
	'uses' => 'PatientController@create',
	'as' => 'patient.create'
]);
Route::post('/admin/patient/store', [
	'uses' => 'PatientController@store',
	'as' => 'patient.store'
]);
Route::get('/admin/patient/edit/{id}', [
	'uses' => 'PatientController@edit',
	'as' => 'patient.edit'
]);
Route::post('/admin/patient/update/{id}', [
	'uses' => 'PatientController@update',
	'as' => 'patient.update'
]);
Route::get('/admin/patient/destroy/{id}', [
	'uses' => 'PatientController@destroy',
	'as' => 'patient.delete'
]);