<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('persons', 'SenzorController@allPersons');
Route::get('getPulse/{pulse}', 'SenzorController@getPulse');
Route::get('getTemperatures/{temperatures}', 'SenzorController@getTemperatures');
Route::get('getAccelerometer/{accelerometer}', 'SenzorController@getAccelerometer');
Route::get('getGyroscope/{gyroscope}', 'SenzorController@getGyroscope');
Route::get('getTension/{tension}', 'SenzorController@getTension');
Route::get('getAllData/{pulse}/{temperatures}/{accelerometer}/{gyroscope}/{tension}', 'SenzorController@getAllData');