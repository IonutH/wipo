<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wipos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pulse')->nullable();
            $table->string('temperatures')->nullable();
            $table->string('accelerometer')->nullable();
            $table->string('gyroscope')->nullable();
            $table->string('tension')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wipos');
    }
}
