<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSenzorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senzors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('steps')->nullable();
            $table->string('rate')->nullable();
            $table->string('activity')->nullable();
            $table->string('sleep')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('senzors');
    }
}
