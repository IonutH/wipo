<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', 'Simfonii de Toamnă') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Vendor Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{asset('css/vendor/accordion/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/lightbox/simplelightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/timepicker/jquery.timepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/owl-carousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/vendor/swiper/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--End VEndor Styles--}}
    @yield('header')
    @yield('headerHomeIncludes')
</head>
<body>
<div id="app">
    <main>
        @yield('content')
        @include('includes.footer_front')
    </main>
</div>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="{{asset('js/vendor/accordion/jquery.accordion.js')}}"></script>
<script src="{{asset('js/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{asset('js/vendor/lightbox/simple-lightbox.min.js')}}"></script>
<script src="{{asset('js/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('js/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/vendor/swiper/swiper.min.js')}}"></script>
<script src="{{asset('js/vendor/scroll-down/scroll-down.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

@yield('page-script')
</body>
</html>
