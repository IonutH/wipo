<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WiPO</title>
    <!--



    -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
    <!-- https://fonts.google.com/specimen/Open+Sans -->
    {{--<link rel="stylesheet" href="css/fontawesome.css">--}}
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/fontawesome.css">
    <link rel="stylesheet" href="{{asset('css/fontawesome.min.css')}}">
    <!-- https://fontawesome.com/ -->
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/fullcalendar.css">
    <link rel="stylesheet" href="{{asset('css/fullcalendar.min.css')}}">
    <!-- https://fullcalendar.io/ -->
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/bootstrap.css">
<!--<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">-->
    <!-- https://getbootstrap.com/ -->
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/tooplate.css">
    <link rel="stylesheet" href="{{asset('css/tooplate.css')}}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('public/css/puls.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/record.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/camera.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/themp.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/compass.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/arows.css')}}">

</head>

<body id="reportsPage">
<div class="" id="home">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-xl navbar-light bg-light">
                    <a class="navbar-brand" href="{{route('admin')}}">
                        <img src="{{asset('public/img/wipo.png')}}" class="nav-icon">
                    </a>
                    <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon" style="margin-left: -12px; margin-top: -2px;"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">WiPO
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('admin.interface')}}">Person Health</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('patient')}}">Persons</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#accounts.html">Accounts</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Settings
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Profile</a>
                                    <a class="dropdown-item" href="#">Billing</a>
                                    <a class="dropdown-item" href="#">Customize</a>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link d-flex" href="#login.html">
                                    <i class="far fa-user mr-2 tm-logout-icon"></i>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- row -->

        <div class="row tm-content-row tm-mt-big">
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block h-100">
                    <h2 class="tm-block-title" id="hr">
                        <img src="{{asset('public/img/heart.gif')}}" height="100px" width="100">

                    </h2>
                    <h3>Heart Rate</h3>
                    <div class="puls">

                        <style>
                            #myCanvas {
                                display: block;
                                border: 1px solid #8C8C8C;
                                margin: 50px auto;
                                background-color: #F2F2F2;
                            }
                        </style>
                        <canvas id="myCanvas" width="600" height="300">
                        </canvas>
                        {{--<!-- Placeholder for the ECG chart -->--}}
                        {{--<div class="jke-ecgChart"></div>--}}

                        {{--<!-- Include dependencies -->--}}
                        {{--<script src="{{asset('public/js/lib/jquery.min.js')}}"></script>--}}
                        {{--<script src="{{asset('public/js/lib/jquery-ui-widget.min.js')}}"></script>--}}
                        {{--<script src="{{asset('public/js/lib/d3.min.js')}}"></script>--}}

                        {{--<!-- Include jke-d3-ecg and initialize the chart -->--}}
                        {{--<script src="{{asset('public/js/jke-d3-ecg.min.js')}}"></script>--}}
                        {{--<script>--}}
                        {{--$('.jke-ecgChart').ecgChart();--}}
                        {{--</script>--}}

                        {{--<!-- Include a data source to feed the ECG chart with data -->--}}
                        {{--<!-- In a real application this should probably be done using a WebSocket -->--}}
                        {{--<script src="{{asset('public/js/datasource.js')}}"></script>--}}
                    </div>

                </div>
            </div>
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block h-100">
                    <div class="row" id="compass">
                        <div class="col-12">
                            <h1 class="tm-block-title" id="compass_title">COMPASS </h1>
                            <div id="notice">
                                {{--<div class="compass">--}}
                                {{--<div class="arrow"></div>--}}
                                {{--<div class="disc" id="compassDiscImg"></div>--}}
                                {{--</div>--}}
                                <img src="{{asset('public/compass/compass.gif')}}">
                                <div class="orientation-data">
                                    <ul>
                                        <li>
                                            Coordonate AX: <span id="cord-x"></span>
                                        </li>
                                        <li>
                                            Coordonate AY: <span id="cord-y"></span>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tm-col tm-col-small">
                <div class="container">
                    <div class="col-sm-12">
                        <ul class="gauge">
                            <li>
                                <p><span class="temp" id="body-temp">20</span><span>&deg;C</span><span class="pl">Body
                                        </span></p>
                            </li>
                            <li class="temp_out">
                                <p><span class="temp">12</span><span>&deg;C</span><span class="pl">Room</span>
                                </p>
                            </li>
                            <div class="row content content-chart">
                                <div class="col-sm-12"></div>
                                <div class="ct-chart vertical-align" id="myChart"></div>
                            </div>
                        </ul>

                    </div>
                </div>
            </div>

            {{--list of person--}}
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block h-100">
                    <div class="row">
                        <div class="col-8">
                            <h2 class="tm-block-title d-inline-block">Persons</h2>

                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route('patient')}}" class="tm-link-black">View All</a>
                        </div>
                    </div>
                    <ol class="tm-list-group tm-list-group-alternate-color tm-list-group-pad-big">
                        <li class="tm-list-group-item">
                            @foreach($patient as $item)
                                {{$item->first_name}} {{$item->last_name}}
                            @endforeach
                        </li>
                    </ol>
                </div>
            </div>
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block h-100">
                    <h2 class="tm-block-title" id="video_title"> VIDEO
                    </h2>
                    <div id="video_container">
                        <div class="select">
                            <label for="audioSource" hidden>Audio input source: </label><select id="audioSource"
                                                                                                hidden></select>
                        </div>

                        <div class="select">
                            <label for="audioOutput" hidden>Audio output destination: </label><select
                                    id="audioOutput" hidden></select>
                        </div>

                        <div class="select">
                            <label for="videoSource">Video source: </label><select id="videoSource"></select>
                        </div>

                        <video id="video" playsinline autoplay></video>


                    </div>

                    <div class="keys " alt="Smiley ">
                        <div class="up">
                            ↑
                        </div>
                        <br/>
                        <div class="left">
                            ←
                        </div>
                        <div class="down">
                            ↓
                        </div>
                        <div class="right">
                            →
                        </div>
                    </div>

                </div>
            </div>
            <div class="tm-col tm-col-small">
{{--                <div class="bg-white tm-block h-100">--}}
{{--                    <h2 class="tm-block-title" id="record_title">Click the button to start/stop recording<br>--}}
{{--                        <button id="recButton"></button>--}}
{{--                        <script src="{{asset('public/js/record.js')}}"></script>--}}

{{--                        <div id="record">--}}

{{--                            --}}{{--<div id="formats" style:overflow="scroll"></div>--}}
{{--                            <h3>Recordings</h3>--}}
{{--                            <!-- inserting these scripts at the end to be able to use all the elements in the DOM -->--}}
{{--                        </div>--}}
{{--                    </h2>--}}
{{--                    <ol id="recordingsList"></ol>--}}
{{--                    <button id="recordButton" hidden></button>--}}
{{--                    <button id="pauseButton" disabled hidden></button>--}}
{{--                    <button id="stopButton" disabled hidden></button>--}}

{{--                    --}}{{--<script src="{{asset('')}}"></script>--}}

{{--                </div>--}}
                <div class="bg-white tm-block h-100">
                    <h2 class="tm-block-title" id="record_title">Click the button to start/stop recording<br>
                        <button id="recButton"></button>

                        <div id="record">

                            <div id="formats" style:overflow="scroll"></div>
                            <h3>Recordings</h3>
                            <!-- inserting these scripts at the end to be able to use all the elements in the DOM -->
                        </div>
                    </h2>
                    <ol id="recordingsList"></ol>
                    <button id="recordButton" hidden></button>
                    <button id="pauseButton" disabled hidden></button>
                    <button id="stopButton" disabled hidden></button>
                </div>
            </div>
        </div>
        <footer class="row tm-mt-small">
            <div class="col-12 font-weight-light">
                <p class="d-inline-block tm-bg-black text-white py-2 px-4">
                    Created by Suceava 2-nd TEAM
                </p>
            </div>
        </footer>
    </div>
</div>
<script src="{{asset('public/js/jquery-3.3.1.min.js')}}"></script>
<!-- https://jquery.com/download/ -->
<script src="{{asset('public/js/moment.min.js')}}"></script>
<!-- https://momentjs.com/ -->
<script src="{{asset('public/js/utils.js')}}"></script>
<script src="{{asset('public/js/Chart.min.js')}}"></script>
<!-- http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('public/js/fullcalendar.min.js')}}"></script>
<!-- https://fullcalendar.io/ -->
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<!-- https://getbootstrap.com/ -->
<script src="{{asset('public/js/tooplate-scripts.js')}}"></script>

<script src="{{asset('public/js/pulse-heartbeat/pulse.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/themp.js')}}"></script>
<script src="{{asset('public/js/record.js')}}"></script>
<script src="{{asset('public/js/recorder.js')}}"></script>
<script src="{{asset('public/js/app.js')}}"></script>
<script src="{{asset('public/js/adapter.js')}}"></script>
<script src="{{asset('public/js/select_video.js')}}" async></script>
<script src="{{asset('public/js/ga.js')}}"></script>
<script src="{{asset('public/js/arows.js')}}"></script>
<script>
  function getBodyTemp () {
    console.log('a intrat in temperatura')
    $.ajax({
      url: '/admin/getBodyTemp',
      dataType: 'json',
    }).done(function (data) {
      console.log(data.temperatures)
      $('#body-temp').html(data.temperatures)
      // alert(mydata)
    }).fail(function () {
    })
  }

  function getCords () {
    console.log('a intrat in coordonate')
    $.ajax({
      url: '/admin/getCords',
      dataType: 'json',
    }).done(function (data) {
      console.log(data)
      $('#cord-x').html(data.accelerometer)
      $('#cord-y').html(data.gyroscope)
      // alert(mydata)
    }).fail(function () {
    })
  }

  function postControl() {
      console.log('a intrat in directei')
      $.ajax({
          type: "POST",
          url: 'admin/getControls',
          dataType: 'json'
      }).done(function (press) {
          console.log(press.direction)

      });
  }

  getCords()
  getBodyTemp()
  window.setInterval(function () {
    /// call your function here
    getCords()
    getBodyTemp()
  }, 2000)  // Change Interval here to test. For eg: 5000 for 5 sec
</script>
<script>
  let ctxLine,
    ctxBar,
    ctxPie,
    optionsLine,
    optionsBar,
    optionsPie,
    configLine,
    configBar,
    configPie,
    lineChart
  barChart, pieChart
  // DOM is ready
  $(function () {
    updateChartOptions()
    drawLineChart() // Line Chart
    drawBarChart() // Bar Chart
    drawPieChart() // Pie Chart
    drawCalendar() // Calendar

    $(window).resize(function () {
      updateChartOptions()
      updateLineChart()
      updateBarChart()
      reloadPage()
    })
  })
</script>
</body>

</html>
