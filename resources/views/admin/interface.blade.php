<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Person Health</title>
    <!--



    -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
    <!-- https://fonts.google.com/specimen/Open+Sans -->
    {{--<link rel="stylesheet" href="css/fontawesome.css">--}}
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/fontawesome.css">
    <link rel="stylesheet" href="{{asset('css/fontawesome.css')}}">
    <!-- https://fontawesome.com/ -->
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/fullcalendar.css">
    <link rel="stylesheet" href="{{asset('css/fullcalendar.css')}}">
    <!-- https://fullcalendar.io/ -->
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/bootstrap.css">
<!--<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">-->
    <!-- https://getbootstrap.com/ -->
    <link rel="stylesheet" href="http://hardandsoft.icosmin.com.ro/public/css/tooplate.css">
    <link rel="stylesheet" href="{{asset('css/tooplate.css')}}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--<link rel="stylesheet" href="{{asset('public/css/puls.css')}}">--}}
    <link rel="stylesheet" href="{{asset('public/css/record.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/camera.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/themp.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/compass.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/arows.css')}}">

    {{--    heartbit--}}
    <style>
        #svg-wrapper {
            width: 500px;
            height: 160px;
            margin: 2em auto;
        }

        svg path {
            fill: none;
            stroke: #000;
            stroke-width: 1.5px;
        }

        svg .axis {
            font-size: 12px;
        }

        svg .axis path {
            display: none;
        }
    </style>
</head>

<body id="reportsPage" class="bg05">
<div class="" id="home">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-xl navbar-light bg-light">
                    <a class="navbar-brand" href="{{route('admin')}}">
                        <img src="{{asset('public/img/wipo.png')}}" class="nav-icon">
                    </a>
                    <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon" style="margin-left: -12px; margin-top: -2px;"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('admin')}}">WiPO
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('admin.interface')}}">Person Health</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('patient')}}">Persons</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="accounts.html">Accounts</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Settings
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Profile</a>
                                    <a class="dropdown-item" href="#">Billing</a>
                                    <a class="dropdown-item" href="#">Customize</a>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link d-flex" href="login.html">
                                    <i class="far fa-user mr-2 tm-logout-icon"></i>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- row -->
        <div class="row tm-content-row tm-mt-big">
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block">
                    <h2 class="tm-block-title" id="hr">
                        <img src="{{asset('public/img/heart.gif')}}" height="100px" width="100">

                    </h2>
                    <h3>Heart Rate</h3>
                    <div class="puls">

                        <style>
                            #myCanvas {
                                display: block;
                                border: 1px solid #8C8C8C;
                                margin: 50px auto;
                                background-color: #F2F2F2;
                            }
                        </style>
                        <canvas id="myCanvas" width="600" height="300">
                        </canvas>
                        {{--<!-- Placeholder for the ECG chart -->--}}
                        {{--<div class="jke-ecgChart"></div>--}}

                        {{--<!-- Include dependencies -->--}}
                        {{--<script src="{{asset('public/js/lib/jquery.min.js')}}"></script>--}}
                        {{--<script src="{{asset('public/js/lib/jquery-ui-widget.min.js')}}"></script>--}}
                        {{--<script src="{{asset('public/js/lib/d3.min.js')}}"></script>--}}

                        {{--<!-- Include jke-d3-ecg and initialize the chart -->--}}
                        {{--<script src="{{asset('public/js/jke-d3-ecg.min.js')}}"></script>--}}
                        {{--<script>--}}
                        {{--$('.jke-ecgChart').ecgChart();--}}
                        {{--</script>--}}

                        {{--<!-- Include a data source to feed the ECG chart with data -->--}}
                        {{--<!-- In a real application this should probably be done using a WebSocket -->--}}
                        {{--<script src="{{asset('public/js/datasource.js')}}"></script>--}}
                    </div>

                </div>
            </div>

            <div class="tm-col tm-col-small">
                <div class="container">
                    <div class="col-sm-12">
                        <ul class="gauge">
                            <li class="temp_in">
                                <p><span class="temp">36</span><span>&deg;C</span><span class="pl">Body
                                        </span></p>
                            </li>
                            <li class="temp_out">
                                <p><span class="temp">23</span><span>&deg;C</span><span class="pl">Room</span>
                                </p>
                            </li>
                            <div class="row content content-chart">
                                <div class="col-sm-12"></div>
                                <div class="ct-chart vertical-align" id="myChart"></div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block">
                    <canvas id="tutorial" width="440" height="220">
                        Canvas not available.
                    </canvas>
                    <div>
                        <form id="drawTemp">
                            <input type="text" id="txtSpeed" name="txtSpeed" value="30" maxlength="2"/>

                            <input type="button" value="Draw" onclick="drawWithInputValue();">
                            <script src="{{asset('public/js/speedometer.js')}}"></script>
                        </form>
                    </div>
                </div>
            </div>
            <div class="map">
                <iframe id="map" width="1600" height="500" allowtransparency="true" opacity="0.1"
                        src="https://maps.google.com/maps?q=Universitatea%20Stefan%20Cel%20Mare%20Suceava&t=&z=17&ie=UTF8&iwloc=&output=embed"></iframe>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="svg-wrapper"></div>
    </div>

{{--    <div class="row">--}}
{{--        <div id="svg-wrapper-sleep"></div>--}}
{{--        <h1>mergeee al doilea</h1>--}}
{{--    </div>--}}
</div>
</div>
<script src="{{asset('public/js/jquery-3.3.1.min.js')}}"></script>
<!-- https://jquery.com/download/ -->
<script src="{{asset('public/js/moment.min.js')}}"></script>
<!-- https://momentjs.com/ -->
<script src="{{asset('public/js/utils.js')}}"></script>
<script src="{{asset('public/js/Chart.min.js')}}"></script>
<!-- http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('public/js/fullcalendar.min.js')}}"></script>
<!-- https://fullcalendar.io/ -->
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<!-- https://getbootstrap.com/ -->
<script src="{{asset('public/js/tooplate-scripts.js')}}"></script>

<script src="{{asset('public/js/pulse-heartbeat/pulse.js')}}"></script>
<script src="{{asset('public/js/themp.js')}}"></script>

{{--heartbeat--}}
<script src="//d3js.org/d3.v3.min.js"></script>
<script type="text/javascript">

    window.onload = function () {

        var svg = null;
        var circle = null;
        var circleTransition = null;
        var latestBeat = null;
        var insideBeat = false;
        var data = [];

        var SECONDS_SAMPLE = 5;
        var BEAT_TIME = 400;
        var TICK_FREQUENCY = SECONDS_SAMPLE * 1000 / BEAT_TIME;
        var BEAT_VALUES = [0, 0, 3, -4, 10, -7, 3, 0, 0];

        var CIRCLE_FULL_RADIUS = 40;
        var MAX_LATENCY = 5000;

        var colorScale = d3.scale.linear()
            .domain([BEAT_TIME, (MAX_LATENCY - BEAT_TIME) / 2, MAX_LATENCY])
            .range(["#6D9521", "#D77900", "#CD3333"]);

        var radiusScale = d3.scale.linear()
            .range([5, CIRCLE_FULL_RADIUS])
            .domain([MAX_LATENCY, BEAT_TIME]);

        function beat() {

            if (insideBeat) return;
            insideBeat = true;

            var now = new Date();
            var nowTime = now.getTime();

            if (data.length > 0 && data[data.length - 1].date > now) {
                data.splice(data.length - 1, 1);
            }

            data.push({
                date: now,
                value: 0
            });

            var step = BEAT_TIME / BEAT_VALUES.length - 2;
            for (var i = 1; i < BEAT_VALUES.length; i++) {
                data.push({
                    date: new Date(nowTime + i * step),
                    value: BEAT_VALUES[i]
                });
            }

            latestBeat = now;

            circleTransition = circle.transition()
                .duration(BEAT_TIME)
                .attr("r", CIRCLE_FULL_RADIUS)
                .attr("fill", "#6D9521");

            setTimeout(function () {
                insideBeat = false;
            }, BEAT_TIME);
        }

        var svgWrapper = document.getElementById("svg-wrapper");
        var margin = {left: 10, top: 10, right: CIRCLE_FULL_RADIUS * 3, bottom: 10},
            width = svgWrapper.offsetWidth - margin.left - margin.right,
            height = svgWrapper.offsetHeight - margin.top - margin.bottom;

        // create SVG
        svg = d3.select('#svg-wrapper').append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.bottom + margin.top)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        circle = svg
            .append("circle")
            .attr("fill", "#6D9521")
            .attr("cx", width + margin.right / 2)
            .attr("cy", height / 2)
            .attr("r", CIRCLE_FULL_RADIUS);

        // init scales
        var now = new Date(),
            fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);

        // create initial set of data
        data.push({
            date: now,
            value: 0
        });

        var x = d3.time.scale()
                .domain([fromDate, new Date(now.getTime())])
                .range([0, width]),
            y = d3.scale.linear()
                .domain([-10, 10])
                .range([height, 0]);

        var line = d3.svg.line()
            .interpolate("basis")
            .x(function (d) {
                return x(d.date);
            })
            .y(function (d) {
                return y(d.value);
            });

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(d3.time.seconds, 1)
            .tickFormat(function (d) {
                var seconds = d.getSeconds() === 0 ? "00" : d.getSeconds();
                return seconds % 10 === 0 ? d.getMinutes() + ":" + seconds : ":" + seconds;
            });

        // add clipPath
        svg.append("defs").append("clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", width)
            .attr("height", height);

        var axis = d3.select("svg").append("g")
            .attr("class", "axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        var path = svg.append("g")
            .attr("clip-path", "url(#clip)")
            .append("path")
            .attr("class", "line");

        svg.select(".line")
            .attr("d", line(data));

        var transition = d3.select("path").transition()
            .duration(100)
            .ease("linear");

        (function tick() {

            transition = transition.each(function () {

                // update the domains
                now = new Date();
                fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);
                x.domain([fromDate, new Date(now.getTime() - 100)]);

                var translateTo = x(new Date(fromDate.getTime()) - 100);

                // redraw the line
                svg.select(".line")
                    .attr("d", line(data))
                    .attr("transform", null)
                    .transition()
                    .attr("transform", "translate(" + translateTo + ")");

                // slide the x-axis left
                axis.call(xAxis);

            }).transition().each("start", tick);
        })();

        setInterval(function () {

            now = new Date();
            fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);

            for (var i = 0; i < data.length; i++) {
                if (data[i].date < fromDate) {
                    data.shift();
                } else {
                    break;
                }
            }

            if (insideBeat) return;

            data.push({
                date: now,
                value: 0
            });

            if (circleTransition != null) {

                var diff = now.getTime() - latestBeat.getTime();

                if (diff < MAX_LATENCY) {
                    circleTransition = circle.transition()
                        .duration(TICK_FREQUENCY)
                        .attr("r", radiusScale(diff))
                        .attr("fill", colorScale(diff));
                }
            }


        }, TICK_FREQUENCY);

        setInterval(function () {
            beat();
        }, 2000);
        beat();
    };

</script>

{{--script sleep time--}}
{{--<script type="text/javascript">--}}

{{--    window.onload = function () {--}}

{{--        var svg = null;--}}
{{--        var circle = null;--}}
{{--        var circleTransition = null;--}}
{{--        var latestBeat = null;--}}
{{--        var insideBeat = false;--}}
{{--        var data = [];--}}

{{--        var SECONDS_SAMPLE = 5;--}}
{{--        var BEAT_TIME = 400;--}}
{{--        var TICK_FREQUENCY = SECONDS_SAMPLE * 1000 / BEAT_TIME;--}}
{{--        var BEAT_VALUES = [0, 0, 3, -4, 10, -7, 3, 0, 0];--}}

{{--        var CIRCLE_FULL_RADIUS = 40;--}}
{{--        var MAX_LATENCY = 5000;--}}

{{--        var colorScale = d3.scale.linear()--}}
{{--            .domain([BEAT_TIME, (MAX_LATENCY - BEAT_TIME) / 2, MAX_LATENCY])--}}
{{--            .range(["#6D9521", "#D77900", "#CD3333"]);--}}

{{--        var radiusScale = d3.scale.linear()--}}
{{--            .range([5, CIRCLE_FULL_RADIUS])--}}
{{--            .domain([MAX_LATENCY, BEAT_TIME]);--}}

{{--        function beat() {--}}

{{--            if (insideBeat) return;--}}
{{--            insideBeat = true;--}}

{{--            var now = new Date();--}}
{{--            var nowTime = now.getTime();--}}

{{--            if (data.length > 0 && data[data.length - 1].date > now) {--}}
{{--                data.splice(data.length - 1, 1);--}}
{{--            }--}}

{{--            data.push({--}}
{{--                date: now,--}}
{{--                value: 0--}}
{{--            });--}}

{{--            var step = BEAT_TIME / BEAT_VALUES.length - 2;--}}
{{--            for (var i = 1; i < BEAT_VALUES.length; i++) {--}}
{{--                data.push({--}}
{{--                    date: new Date(nowTime + i * step),--}}
{{--                    value: BEAT_VALUES[i]--}}
{{--                });--}}
{{--            }--}}

{{--            latestBeat = now;--}}

{{--            circleTransition = circle.transition()--}}
{{--                .duration(BEAT_TIME)--}}
{{--                .attr("r", CIRCLE_FULL_RADIUS)--}}
{{--                .attr("fill", "#6D9521");--}}

{{--            setTimeout(function () {--}}
{{--                insideBeat = false;--}}
{{--            }, BEAT_TIME);--}}
{{--        }--}}

{{--        var svgWrapper = document.getElementById("svg-wrapper-sleep");--}}
{{--        var margin = {left: 10, top: 10, right: CIRCLE_FULL_RADIUS * 3, bottom: 10},--}}
{{--            width = svgWrapper.offsetWidth - margin.left - margin.right,--}}
{{--            height = svgWrapper.offsetHeight - margin.top - margin.bottom;--}}

{{--        // create SVG--}}
{{--        svg = d3.select('#svg-wrapper-sleep').append("svg")--}}
{{--            .attr("width", width + margin.left + margin.right)--}}
{{--            .attr("height", height + margin.bottom + margin.top)--}}
{{--            .append("g")--}}
{{--            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");--}}

{{--        circle = svg--}}
{{--            .append("circle")--}}
{{--            .attr("fill", "#6D9521")--}}
{{--            .attr("cx", width + margin.right / 2)--}}
{{--            .attr("cy", height / 2)--}}
{{--            .attr("r", CIRCLE_FULL_RADIUS);--}}

{{--        // init scales--}}
{{--        var now = new Date(),--}}
{{--            fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);--}}

{{--        // create initial set of data--}}
{{--        data.push({--}}
{{--            date: now,--}}
{{--            value: 0--}}
{{--        });--}}

{{--        var x = d3.time.scale()--}}
{{--                .domain([fromDate, new Date(now.getTime())])--}}
{{--                .range([0, width]),--}}
{{--            y = d3.scale.linear()--}}
{{--                .domain([-10, 10])--}}
{{--                .range([height, 0]);--}}

{{--        var line = d3.svg.line()--}}
{{--            .interpolate("basis")--}}
{{--            .x(function (d) {--}}
{{--                return x(d.date);--}}
{{--            })--}}
{{--            .y(function (d) {--}}
{{--                return y(d.value);--}}
{{--            });--}}

{{--        var xAxis = d3.svg.axis()--}}
{{--            .scale(x)--}}
{{--            .orient("bottom")--}}
{{--            .ticks(d3.time.seconds, 1)--}}
{{--            .tickFormat(function (d) {--}}
{{--                var seconds = d.getSeconds() === 0 ? "00" : d.getSeconds();--}}
{{--                return seconds % 10 === 0 ? d.getMinutes() + ":" + seconds : ":" + seconds;--}}
{{--            });--}}

{{--        // add clipPath--}}
{{--        svg.append("defs").append("clipPath")--}}
{{--            .attr("id", "clip")--}}
{{--            .append("rect")--}}
{{--            .attr("width", width)--}}
{{--            .attr("height", height);--}}

{{--        var axis = d3.select("svg").append("g")--}}
{{--            .attr("class", "axis")--}}
{{--            .attr("transform", "translate(0," + height + ")")--}}
{{--            .call(xAxis);--}}

{{--        var path = svg.append("g")--}}
{{--            .attr("clip-path", "url(#clip)")--}}
{{--            .append("path")--}}
{{--            .attr("class", "line");--}}

{{--        svg.select(".line")--}}
{{--            .attr("d", line(data));--}}

{{--        var transition = d3.select("path").transition()--}}
{{--            .duration(100)--}}
{{--            .ease("linear");--}}

{{--        (function tick() {--}}

{{--            transition = transition.each(function () {--}}

{{--                // update the domains--}}
{{--                now = new Date();--}}
{{--                fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);--}}
{{--                x.domain([fromDate, new Date(now.getTime() - 100)]);--}}

{{--                var translateTo = x(new Date(fromDate.getTime()) - 100);--}}

{{--                // redraw the line--}}
{{--                svg.select(".line")--}}
{{--                    .attr("d", line(data))--}}
{{--                    .attr("transform", null)--}}
{{--                    .transition()--}}
{{--                    .attr("transform", "translate(" + translateTo + ")");--}}

{{--                // slide the x-axis left--}}
{{--                axis.call(xAxis);--}}

{{--            }).transition().each("start", tick);--}}
{{--        })();--}}

{{--        setInterval(function () {--}}

{{--            now = new Date();--}}
{{--            fromDate = new Date(now.getTime() - SECONDS_SAMPLE * 1000);--}}

{{--            for (var i = 0; i < data.length; i++) {--}}
{{--                if (data[i].date < fromDate) {--}}
{{--                    data.shift();--}}
{{--                } else {--}}
{{--                    break;--}}
{{--                }--}}
{{--            }--}}

{{--            if (insideBeat) return;--}}

{{--            data.push({--}}
{{--                date: now,--}}
{{--                value: 0--}}
{{--            });--}}

{{--            if (circleTransition != null) {--}}

{{--                var diff = now.getTime() - latestBeat.getTime();--}}

{{--                if (diff < MAX_LATENCY) {--}}
{{--                    circleTransition = circle.transition()--}}
{{--                        .duration(TICK_FREQUENCY)--}}
{{--                        .attr("r", radiusScale(diff))--}}
{{--                        .attr("fill", colorScale(diff));--}}
{{--                }--}}
{{--            }--}}


{{--        }, TICK_FREQUENCY);--}}

{{--        setInterval(function () {--}}
{{--            beat();--}}
{{--        }, 2000);--}}
{{--        beat();--}}
{{--    };--}}

{{--</script>--}}
{{--end script sleep time--}}

<script>
    let ctxLine,
        ctxBar,
        ctxPie,
        optionsLine,
        optionsBar,
        optionsPie,
        configLine,
        configBar,
        configPie,
        lineChart;
    barChart, pieChart;
    // DOM is ready
    $(function () {
        updateChartOptions();
        drawLineChart(); // Line Chart
        drawBarChart(); // Bar Chart
        drawPieChart(); // Pie Chart
        drawCalendar(); // Calendar

        $(window).resize(function () {
            updateChartOptions();
            updateLineChart();
            updateBarChart();
            reloadPage();
        });
    })
</script>
</body>

</html>
