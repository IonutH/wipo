<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Utilizatori </title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
    <!-- https://fonts.google.com/specimen/Open+Sans -->
    <link rel="stylesheet" href="{{asset('public/css/fontawesome.css')}}">
    <!-- https://fontawesome.com/ -->
    <link rel="stylesheet" href="{{asset('public/css/bootstrap.css')}}">
    <!-- https://getbootstrap.com/ -->
    <link rel="stylesheet" href="{{asset('public/css/tooplate.css')}}">
</head>

<body id="reportsPage" class="bg02">
<div class="" id="home">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-xl navbar-light bg-light">
                    <a class="navbar-brand" href="{{route('admin')}}">
                        <img src="{{asset('public/img/wipo.png')}}" class="nav-icon">
                    </a>
                    <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('admin')}}">WiPO
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('admin.interface')}}">Person Health</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{route('patient')}}">Persons</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="accounts.html">Accounts</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Settings
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Profile</a>
                                    <a class="dropdown-item" href="#">Billing</a>
                                    <a class="dropdown-item" href="#">Customize</a>
                                </div>
                            </li>
                        </ul>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link d-flex" href="login.html">
                                    <i class="far fa-user mr-2 tm-logout-icon"></i>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- row -->
        <div class="row tm-content-row tm-mt-big">
            <div class="col-xl-12 col-lg-12 tm-md-12 tm-sm-12 tm-col">
                <div class="bg-white tm-block h-100">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col-12 headings">
                            <h2 class="tm-block-title d-inline-block">Persons</h2>

                        </div>
                        <div class="col-md-4 col-sm-6 col-12 headings">
                            <a href="{{route('patient.create')}}" class="btn btn-small btn-primary btn-person btn-person-left" style="margin-right: 0">Add a person</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped tm-table-striped-even mt-3">
                            <thead>
                            <tr class="tm-bg-gray">
                                {{--<th scope="col">&nbsp;</th>--}}
                                <th scope="col">Person ID</th>
                                <th scope="col">Person Name</th>
                                <th scope="col" class="text-center"> Age</th>
                                <th scope="col" class="text-center">Gender</th>
                                <th scope="col" class="text-center">Blood Type</th>
                                <th scope="col" class="text-center">Phone</th>
                                <th scope="col" class="text-center">Mail</th>
                                <th scope="col" class="text-center">Address</th>
                                <th scope="col" class="text-center">Observations</th>
                                <th scope="col" class="text-center">&nbsp;Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($patient as $item)
                                <tr>
                                    <td>
                                        {{$item->id}}
                                    </td>
                                    <td>
                                        {{$item->first_name}} {{$item->last_name}}
                                    </td>
                                    <td>
                                        {{$item->age}}
                                    </td>
                                    <td>
                                        {{$item->sex}}
                                    </td>
                                    <td>
                                        {{$item->blood_type}}
                                    </td>
                                    <td>
                                        {{$item->phone}}
                                    </td>
                                    <td>
                                        {{$item->mail}}
                                    </td>
                                    <td>
                                        {{$item->address}}
                                    </td>
                                    <td>
                                        {{$item->observations}}
                                    </td>
                                    <td style="display: inline-flex">
                                        <a href="{{route('patient.edit', ['id' => $item->id])}}">
                                            <button class="btn btn-primary btn-person">
                                                Edit
                                            </button>
                                        </a>

                                        <button type="button" class="btn btn-danger btn-person"
                                                data-href="{{route('patient.delete', ['id' => $item->id])}}"
                                                data-toggle="modal" data-target="#confirm-delete">Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-notify modal-danger" role="document">
        <div class="modal-content" style="background-color: #F8D4D4;">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete the person</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                Are you sure you want delete this person??
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, keep it</button>
                <a class="btn btn-danger btn-ok">Yes, delete</a>
            </div>

        </div>
    </div>
</div>

<script src="{{asset('public/js/jquery-3.3.1.min.js')}}"></script>
<!-- https://jquery.com/download/ -->
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<!-- https://getbootstrap.com/ -->
<script>
    $(function () {
        $('.tm-product-name').on('click', function () {
            window.location.href = "edit-product.html";
        });
    })
</script>

<script>
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
</script>

</body>

</html>
