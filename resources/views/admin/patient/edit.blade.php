<!DOCTYPE html>
<html lang="en">

<head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>New Person</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
        <!-- https://fonts.google.com/specimen/Open+Sans -->
        <link rel="stylesheet" href="{{asset('public/css/fontawesome.css')}}">
        <!-- https://fontawesome.com/ -->
        <link rel="stylesheet" href="{{asset('public/jquery-ui-datepicker/jquery-ui.min.css')}}" type="text/css" />
        <!-- http://api.jqueryui.com/datepicker/ -->
        <link rel="stylesheet" href="{{asset('public/css/bootstrap.css')}}">
        <!-- https://getbootstrap.com/ -->
        <link rel="stylesheet" href="{{asset('public/css/tooplate.css')}}">
    </head>
</head>

<body class="bg02">
<div class="container">
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-xl navbar-light bg-light">
                <a class="navbar-brand" href="{{route('admin')}}">
                    <img src="{{asset('public/img/wipo.png')}}" class="nav-icon">
                </a>
                <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('admin')}}">WiPO
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Status
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Heart Rate</a>
                                <a class="dropdown-item" href="#">Body Temperature</a>
                                <a class="dropdown-item" href="#">State</a>
                            </div>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('patient')}}">Persons</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="accounts.html">Accounts</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Settings
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Profile</a>
                                <a class="dropdown-item" href="#">Billing</a>
                                <a class="dropdown-item" href="#">Customize</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link d-flex" href="login.html">
                                <i class="far fa-user mr-2 tm-logout-icon"></i>
                                <span>Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- row -->
    <div class="row tm-mt-big">
        <div class="col-xl-12">
            <div class="bg-white tm-block" style="margin-bottom: 50px;">
                <div class="row">
                    <div class="col-12">
                        <h2 class="tm-block-title">Edit Person</h2>
                    </div>
                </div>
                <div class="row mt-4 tm-edit-product-row">
                    <div class="col-xl-12">
                        <form action='{{route('patient.update',['id'=>$patient->id])}}' method="post"
                              enctype="multipart/form-data" class="tm-edit-product-form">
                            {{ csrf_field() }}
                            <div class="input-group mb-3">
                                <label for="first_name" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">
                                    First name
                                </label>
                                <input placeholder="First name" value="{{$patient->first_name}}" id="first_name"
                                       name="first_name" type="text"
                                       class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7">
                            </div>
                            <div class="input-group mb-3">
                                <label for="last_name" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">
                                    Last name
                                </label>
                                <input placeholder="Last name" value="{{$patient->last_name}}" id="last_name"
                                       name="last_name" type="text"
                                       class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7">
                            </div>
                            <div class="input-group mb-3">
                                <label for="age" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">
                                    Age
                                </label>
                                <input placeholder="Age" value="{{$patient->age}}" id="age"
                                       name="age" type="text"
                                       class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7">
                            </div>
                            <div class="input-group mb-3">
                                <label for="sex"
                                       class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Gender</label>
                                <select class="custom-select col-xl-9 col-lg-8 col-md-8 col-sm-7" id="sex" name="sex">
                                    <option selected>{{$patient->sex}}</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <label for="blood_type"
                                       class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Blood type</label>
                                <select class="custom-select col-xl-9 col-lg-8 col-md-8 col-sm-7" id="blood_type" name="blood_type">
                                    <option selected>{{$patient->blood_type}}</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="AB">AB</option>
                                    <option value="O">O</option>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <label for="phone" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">
                                    Phone
                                </label>
                                <input placeholder="Phone" value="{{$patient->phone}}" id="phone"
                                       name="phone" type="text"
                                       class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7">
                            </div>
                            <div class="input-group mb-3">
                                <label for="mail" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">
                                    E-mail
                                </label>
                                <input placeholder="E-mail" value="{{$patient->mail}}" id="mail"
                                       name="mail" type="text"
                                       class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7">
                            </div>
                            <div class="input-group mb-3">
                                <label for="address" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">
                                    Address
                                </label>
                                <input placeholder="Address" value="{{$patient->address}}" id="address"
                                       name="address" type="text"
                                       class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7">
                            </div>
                            <div class="input-group mb-3">
                                <label for="observations"
                                       class="col-xl-4 col-lg-4 col-md-4 col-sm-5 mb-2">Observations</label>
                                <textarea class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7" rows="3" id="observations" name="observations"
                                           required>{!! $patient->observations !!}</textarea>
                            </div>
                        </form>
                        <div class="col-xl-4 col-lg-4 col-md-12 mx-auto mb-4">
                            <img src="{{asset($patient->image)}}" alt="Profile Image" class="img-fluid mx-auto d-block">
                            <div class="custom-file mt-3 mb-3">
                                <input id="fileInput" name="image" type="file" style="display:none;" />
                                <input type="button" class="btn btn-primary btn-person" value="Upload ..."
                                       onclick="document.getElementById('fileInput').click();" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('public/js/jquery-3.3.1.min.js')}}"></script>
<!-- https://jquery.com/download/ -->
<script src="{{asset('public/jquery-ui-datepicker/jquery-ui.min.js')}}"></script>
<!-- https://jqueryui.com/download/ -->
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<!-- https://getbootstrap.com/ -->
<script>
    $(function () {
        $('#expire_date').datepicker();
    });
</script>
</body>

</html>
